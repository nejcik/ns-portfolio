class PortfoliosController < ApplicationController
	before_action :set_portfolios, only: [:show, :edit, :update, :destroy]
  before_action :authorize_admin, only: [:new,:edit,:update,:destroy]
  layout 'portfolio'

	def index
		@portfolio_items=Portfolio.by_position
    @page_title = "Portfolio"
	end

  def sort
    params[:order].each do |k,v|
      Portfolio.find(v[:id]).update(position: v[:position])
    end

    # render nothing: true
    head :ok
    # render body: nil
  end

	# new only renders, not creates!
	def new
    @page_title = "New Potfolio "

		# comment out below line and in new.html.erb use for_with()
		@portfolio_item = Portfolio.new

    #hard code for giving 3 technologies to each new portfolio item
    # 3.times {@portfolio_item.technology_tags.build}
	end

	def create
		@portfolio_item = Portfolio.new(portfolio_item_params)

   	 respond_to do |format|
      if @portfolio_item.save
        format.html { redirect_to portfolios_path, notice: 'Item to your portfolio was successfully created.' }
        # format.json { render :show, status: :created, location: @portfolio_item } WERE NOT USING JSON
      else
        format.html { render :new }
        # format.json { render json: @portfolio_item.errors, status: :unprocessable_entity }
      end
     end
	end

	def edit
    @page_title = "Edit Portfolio "

	end

	 def update
    respond_to do |format|
      if @portfolio_item.update(portfolio_item_params)
        format.html { redirect_to portfolios_path, notice: 'Your item was successfully updated.' }
        # format.json { render :show, status: :ok, location: @portfolio_item }
      else
        format.html { render :edit }
        # format.json { render json: @portfolio_item.errors, status: :unprocessable_entity }  
      end
     end
  	end

  	def show
      @page_title = @portfolio_item.title
  	end

  def destroy
    @portfolio_item.destroy
    respond_to do |format|
      format.html { redirect_to portfolios_url, notice: 'Your item was successfully destroyed.' }
      # format.json { head :no_content }
    end
  end

  private
	
	# Never trust parameters from the scary internet, only allow the white list through.
    def portfolio_item_params
      params.require(:portfolio).permit(:title, 
                                        :subtitle, 
                                        :content,
                                        :main_image,
                                        :button_image,
                                        technology_tags_attributes:  [:id, :name, :_destroy]                              
                                        )
    end

    def set_portfolios
    	@portfolio_item = Portfolio.find(params[:id])
    end


end

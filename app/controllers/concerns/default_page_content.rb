module DefaultPageContent
	extend ActiveSupport::Concern

	included do 
		before_action :set_page_default
	end

	def set_page_default
		@base_title = " Natalia Skowronek"
		@page_title = "My Portfolio "

		@seo_keywords = "Natalia Skowronek portfolio"
	end
end
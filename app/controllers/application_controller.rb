class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include DefaultPageContent


	def authorize_admin
		# alert_generator "No access"
		
		redirect_to  root_path unless current_user && current_user.admin
		flash[:notice]= "No Access"

	end
end

class CurriculumsController < ApplicationController
		before_action :set_curriculum, only: [:show, :edit, :update, :destroy]
    before_action :authorize_admin, only: [:new,:edit,:update,:destroy]
    layout "curriculum"

	# For all other people <3
  def presentation
    @page_title = "Presentation "
    
    @cv_presentations=Curriculum.where(category_id: 1)
    
  end

  def education 
    @page_title = "Education "
    @cv_educaations=Curriculum.where(category_id: 2) 
  end

  def experience
    @page_title = "Experience "    
    @cv_experiences=Curriculum.where(category_id: 3)
  end

  def other
    @page_title = "Other "
    @cv_others=Curriculum.where(category_id: 4)
  end


	# Used for ADMIN:
	def index
    @page_title = "About "
		@curriculum_items=Curriculum.all
	end

	# new only renders, not creates!
	def new
    @page_title = "New CV "
    # byebug
		# comment out below line and in new.html.erb use for_with()
		@curriculum_item = Curriculum.new
	end

	def create
		@curriculum_item = Curriculum.new(curriculum_item_params)
   	 respond_to do |format|
      # byebug
      if @curriculum_item.save
        format.html { redirect_to about_path, notice: 'Item to your curriculum was successfully created.' }
        # format.json { render :show, status: :created, location: @curriculum_item } WERE NOT USING JSON
      else
        format.html { render :new }
        # format.json { render json: @curriculum_item.errors, status: :unprocessable_entity }
      end
     end
	end

	def edit
    @page_title = "Edit CV "
	end

	 def update
    respond_to do |format|
      if @curriculum_item.update(curriculum_item_params)
        format.html { redirect_to about_path, notice: 'Your item was successfully updated.' }
        # format.json { render :show, status: :ok, location: @curriculum_item }
      else
        format.html { render :edit }
        # format.json { render json: @curriculum_item.errors, status: :unprocessable_entity }  
      end
     end
  	end

  	def show
      @page_title = @curriculum_item.title
  	end

  def destroy
    @curriculum_item.destroy
    respond_to do |format|
      format.html { redirect_to curriculums_url, notice: 'Your item was successfully destroyed.' }
      # format.json { head :no_content }
    end
  end

  private
	
	# Never trust parameters from the scary internet, only allow the white list through.
    def curriculum_item_params
      params.require(:curriculum).permit(:title, 
                                        :subtitle, 
                                        :content,                        
                                     )
    end

    def set_curriculum
	     @curriculum_item = Curriculum.find(params[:id])  
	 end

end

class Category < ApplicationRecord
	has_many :curriculums

	validates_presence_of :title
end

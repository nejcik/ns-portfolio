class Portfolio < ApplicationRecord
	validates_presence_of :title, :content#, :main_image, :button_image
	has_many :technology_tags
   accepts_nested_attributes_for :technology_tags, 
							     allow_destroy: true,
                                 reject_if: lambda { |attrs| attrs['name'].blank?}
 
	def self.by_position
		order("position ASC")
	end

  mount_uploader :button_image, PortfolioUploader
  mount_uploader :main_image, PortfolioUploader

  
end

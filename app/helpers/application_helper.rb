module ApplicationHelper
	def login_helper(styles) 
		if current_user
			link_to "LOG OUT", destroy_user_session_path,  class: styles  , method: :delete
		else
			(link_to "SIGN UP",  new_user_registration_path, class: styles )+ " ".html_safe
			(link_to "LOGIN",  new_user_session_path,  class: styles  )
		end
	end

	def admin_page_helper(styles)
		if current_user
			link_to "ADMIN", admin_path, class: styles
		end
	end

	def crud_helper(option,name =nil, path=nil, obj = nil)
		if current_user
			
			if option == "New" or option =="new"	
				link_to "#{option.capitalize} #{name}", path
			elsif option == "Edit" or option =="edit"
				link_to "#{option.capitalize}", path
			elsif option == "Delete" or option == "delete"
				link_to "#{option.capitalize}", obj, method: :delete, data: { confirm: 'Are you sure?' } 					
			end
		end
	end

	def sortable_helper
		if current_user
			"sortable"
		end
	end

# ALERTS

  def alerts
     alert = (flash[:alert] || flash[:error] || flash[:notice])
 
     if alert
       alert_generator alert
     end
   end
 
   def alert_generator msg
     js add_gritter(msg, title: "NS Portfolio", sticky: false)
   end


end

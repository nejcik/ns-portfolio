# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create!(
	title: "Presentation")

Category.create!(
	title: "Education")

Category.create!(
	title: "Experience")

Category.create!(
	title: "Other")

6.times do |p_item|
	Portfolio.create!(
		title: "Item: #{p_item}",
		subtitle: "RoR",
		content: "Kaligula kazał umorzyć wszystkie procesy polityczne, odwołał osoby zesłane z przyczyn politycznych, spalił publicznie 
			   akta procesu matki i braci oraz rozkazał opublikować dzieła dawnych historyków opozycyjnych. Wprowadził ulgi podatkowe, 
			   ponowił publikowanie rachunków państwowych. Pojechał na Pandaterię i Pontię po prochy matki Agrypiny i brata Nerona, 
			   aby złożyć je w mauzoleum cesarskim. Swoim siostrom i babce Antonii przyznał specjalne przywileje.",
	)
end

puts "6 portfolio items created"

3.times do |technology|
	# instead of:
	# Technology.create!(
	# 	name: "Technology #{technology}",
	# 	portfolio_id: Portfolio.last.id
	# 	)

	Portfolio.last.technology_tags.create!(
		name: "Technology #{technology}"
		)
end

puts "3 technologies added  to last portfolio item"

2.times do |cv_item|
	Curriculum.create!(
		title: "Item: #{cv_item}",
		subtitle: "ABOUT ME",
		content: "Kaligula kazał umorzyć wszystkie procesy polityczne, specjalne przywileje.",
		category_id: 1
	)
end

2.times do |cv_item|
	Curriculum.create!(
		title: "Item: #{cv_item+2}",
		subtitle: "nEDUCATION",
		content: " specjalne przywileje Kaligula kazał umorzyć wszystkie procesy polityczne,",
		category_id: 2
	)
end

2.times do |cv_item|
	Curriculum.create!(
		title: "Item: #{cv_item+4}",
		subtitle: "EXPERIENCE",
		content: " specjalne przywileje Kaligula kazał umorzyć wszystkie procesy polityczne,",
		category_id: 3
	)
end

	Curriculum.create!(
		title: "Item: 7",
		subtitle: "OTHER",
		content: "umorzyć specjalne przywileje Kaligula kazał, wszystkie procesy polityczne,",
		
		category_id: 4
	)

	puts "7 CV items added"
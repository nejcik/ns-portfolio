class CreatePortfolios < ActiveRecord::Migration[5.1]
  def change
    create_table :portfolios do |t|
      t.string :title
      t.string :subtitle
      t.text :content
      t.text :main_image
      t.text :button_image

      t.timestamps
    end
  end
end

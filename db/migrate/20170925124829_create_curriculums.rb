class CreateCurriculums < ActiveRecord::Migration[5.1]
  def change
    create_table :curriculums do |t|
      t.text :title
      t.text :subtitle
      t.text :content
      t.text :image

      t.timestamps
    end
  end
end

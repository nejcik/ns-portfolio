class AddCategoryReferenceToCurriculums < ActiveRecord::Migration[5.1]
  def change
    add_reference :curriculums, :category, foreign_key: true
  end
end

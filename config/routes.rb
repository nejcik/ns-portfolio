Rails.application.routes.draw do
  
  devise_for :users, controllers: { registrations: "registrations"}
  
  resources :curriculums, except: [:index]
  get 'about_me', to: 'curriculums#index', as: 'about'
  get 'presentation', to: 'curriculums#presentation', as: 'presentation'
  get 'education', to: 'curriculums#education', as: 'education'
  get 'experience', to: 'curriculums#experience', as: 'experience'
  get 'other', to: 'curriculums#other', as: 'other'

  resources :portfolios do
    put :sort, on: :collection
  
     # get 'portfolio/:id', to: 'portfolios#show', as: 'portfolio_show'

  end
  # get 'portfolio', to: 'portfolios#index', as: 'portfolios'
  # get 'portfolio', to: 'portfolios#index'

  

  root to: 'pages#home'
  # Changing behavour of about me => pages#about is now ADMIN PAGE 
  get 'nns-admin-page', to: 'pages#admin', as: 'admin'
  get 'credit',to: 'pages#credit', as: 'credit'
  get 'contact', to: 'pages#contact'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
